/**
 * Copyright © 2018 The Thingsboard Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.rule.engine.node.action;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.thingsboard.rule.engine.api.RuleEngineDeviceRpcRequest;
import org.thingsboard.rule.engine.api.RuleNode;
import org.thingsboard.rule.engine.api.TbContext;
import org.thingsboard.rule.engine.api.TbNode;
import org.thingsboard.rule.engine.api.TbNodeConfiguration;
import org.thingsboard.rule.engine.api.TbNodeException;
import org.thingsboard.rule.engine.api.TbRelationTypes;
import org.thingsboard.rule.engine.api.util.TbNodeUtils;
import org.thingsboard.server.common.data.DataConstants;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.plugin.ComponentType;
import org.thingsboard.server.common.msg.TbMsg;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j @RuleNode(
        type = ComponentType.ACTION,
        name = "send rpc downlink command",
        configClazz = TbRpcDownlinkNodeConfiguration.class,
        nodeDescription = "Sends RPC request to gateway",
        nodeDetails = "If request is successful send response via <b>Success</b> chain, otherwise <b>Failure</b> chain is used.",
        uiResources = {"static/rulenode/custom-nodes-config.js"}
        // configDirective = "tbFilterNodeCheckKeyConfig"
)
public class TbRpcDownlinkNode implements TbNode {

    private final Random random = new Random();
    private final Gson gson = new Gson();
    private final JsonParser jsonParser = new JsonParser();
    private TbRpcDownlinkNodeConfiguration config;

    @Override
    public void init(TbContext ctx, TbNodeConfiguration configuration) throws TbNodeException {
        this.config = TbNodeUtils.convert(configuration, TbRpcDownlinkNodeConfiguration.class);
    }

    @Override
    public void onMsg(TbContext ctx, TbMsg msg) {
        String method = config.getMethod();
        String sensorId = config.getSensorId();

        String tmp;
        Device device = ctx.getDeviceService().findDeviceByTenantIdAndName(ctx.getTenantId(), sensorId);
        JsonObject paramsObject;

        try {
            paramsObject = jsonParser.parse(config.getParams()).getAsJsonObject();
            if (msg.getOriginator().getEntityType() != EntityType.DEVICE) {
                ctx.tellFailure(msg, new RuntimeException("Message originator is not a device entity!"));
            } else if (!StringUtils.hasLength(method)) {
                ctx.tellFailure(msg, new RuntimeException("Method is not defined!"));
            } else if (!method.matches("setProperty|getProperty")) {
                ctx.tellFailure(msg, new RuntimeException("Method provided is invalid/not allowed."));
            } else if (!StringUtils.hasLength(sensorId)) {
                ctx.tellFailure(msg, new RuntimeException("SensorId is not provided!"));
            } else if (device == null) {
                ctx.tellFailure(msg, new RuntimeException("Invalid sensorId provided!"));
            } else {

                int requestId = random.nextInt();
                boolean restApiCall = msg.getType().equals(DataConstants.RPC_CALL_FROM_SERVER_TO_DEVICE);

                tmp = msg.getMetaData().getValue("oneway");
                boolean oneway = StringUtils.hasLength(tmp) && Boolean.parseBoolean(tmp);

                tmp = msg.getMetaData().getValue("requestUUID");
                UUID requestUUID = StringUtils.hasLength(tmp) ? UUID.fromString(tmp) : Uuids.timeBased();
                tmp = msg.getMetaData().getValue("originServiceId");
                String originServiceId = !StringUtils.hasLength(tmp) ? tmp : null;

                tmp = msg.getMetaData().getValue("expirationTime");
                long expirationTime = StringUtils.hasLength(tmp) ? Long.parseLong(tmp) :
                        (System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(config.getTimeoutInSeconds()));

                String params;
                if (paramsObject.isJsonPrimitive()) {
                    params = paramsObject.getAsString();
                } else {
                    params = gson.toJson(paramsObject);
                }

                RuleEngineDeviceRpcRequest request = RuleEngineDeviceRpcRequest.builder()
                        .oneway(oneway)
                        .method(method)
                        .body(params)
                        .tenantId(ctx.getTenantId())
                        .deviceId(device.getId())
                        .requestId(requestId)
                        .requestUUID(requestUUID)
                        .originServiceId(originServiceId)
                        .expirationTime(expirationTime)
                        .restApiCall(restApiCall)
                        .build();

                ctx.getRpcService().sendRpcRequestToDevice(request, ruleEngineDeviceRpcResponse -> {
                    if (!ruleEngineDeviceRpcResponse.getError().isPresent()) {
                        TbMsg next = ctx.newMsg(msg.getQueueName(), msg.getType(), msg.getOriginator(), msg.getMetaData(), ruleEngineDeviceRpcResponse.getResponse().orElse("{}"));
                        ctx.enqueueForTellNext(next, TbRelationTypes.SUCCESS);
                    } else {
                        TbMsg next = ctx.newMsg(msg.getQueueName(), msg.getType(), msg.getOriginator(), msg.getMetaData(), wrap(ruleEngineDeviceRpcResponse.getError().get().name()));
                        ctx.tellFailure(next, new RuntimeException(ruleEngineDeviceRpcResponse.getError().get().name()));
                    }
                });
                ctx.ack(msg);
            }
        } catch (Exception e) {
            ctx.tellFailure(msg, new RuntimeException("Params are not valid JSON object"));
        }
    }

    @Override
    public void destroy() {
    }

    private String wrap(String body) {
        JsonObject json = new JsonObject();
        json.addProperty("error", body);
        return gson.toJson(json);
    }
}
